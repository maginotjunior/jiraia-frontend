const path = require('path');
const process = require('process');
const express = require('express');
const exphbs = require('express-handlebars');
const app = express();
const port = process.env.PORT;
const ip = process.env.IP;



app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
});


app.engine('.hbs', exphbs({
  //defaultLayout: 'main',
  extname: '.hbs',
  //layoutsDir: path.join(__dirname, 'public')
}))
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'public'));

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
app.get('/', (req, res) => {
  path.join(__dirname + '/index.html')
});
app.get('/index.html', (req, res) => {
  path.join(__dirname + '/index.html')
});
app.get('/engine.html', (req, res) => {
  path.join(__dirname + '/engine.html')
});
*/

app.use('/404', function(req, res){
	res.status(404).sendFile(path.join(__dirname, '404.html'));
});

app.use(function(req, res, next){
    res.redirect('/404');
});