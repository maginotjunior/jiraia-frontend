/*
	@author Maginot Junior <maginot.junior at gmail.com>
	@license Jiraia é um software desenvolvido apenas para demonstração. Não é
	permitido seu uso sob nenhuma outra forma sem autorização do autor.
*/
var Jiraia = (function(){
	function Jiraia(){
		
		this.baseHost = null;
		this.tokenEndPoint = '/api/token/';
		this.saveSessionEndPoint = '/api/session/save/';
		
		this._triggers = [];
		
	}
	
	Jiraia.prototype.configure = function(options){
		if(options.hasOwnProperty('server')){
			this.setBaseHost(options.server);
		}
		this.options = options;
	};
	
	Jiraia.prototype.setBaseHost = function(host){
		this.baseHost = host;  
	};
	Jiraia.prototype.getBaseHost = function(host){
		return !this.baseHost ? window.location.host : this.baseHost;
	};
	
	Jiraia.prototype.getTokenEndPoint = function(){
		return this.tokenEndPoint;  
	};
	Jiraia.prototype.getSaveSessionEndPoint = function(){
		return this.saveSessionEndPoint;  
	};
	
	
	/* @return bool */
	Jiraia.prototype.nosession = function(){
		// @todo keep some sessions stickyness
		return false;  
	};
	
	/* @return int */
	Jiraia.prototype._findInStorage = function(done){
		try {
			if (typeof(window.localStorage) !== "undefined") {
				var session = window.localStorage.getItem("rd-sess");
				if(!session){
					return null;
				}
				console.log('storage');
				console.log(session);
				return session;
			} else {
				return null;
			}
		} catch(e){
			console.error(e);
			return null;
		}
	};
	
	/* @return object */
	Jiraia.prototype._findInCookies = function(){
		try {
			console.log('cookies');
			var session = document.cookie.replace(/(?:(?:^|.*;\s*)rd-sess\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			console.log(session);
			return !session ? null : session;
		} catch(e){
			console.error(e);
			return null;
		}
	};
	
	/* @return void */
	Jiraia.prototype.__createLocalSession = function(token){
		window.localStorage.setItem("rd-sess", token);
	};
	
	/* @return void */
	Jiraia.prototype.__createCookiesSession = function(token){
		document.cookie = "rd-sess="+token;
	};
	
	/* @return void 
	 * @callback f(String err, String token) 
	 */
	Jiraia.prototype.__getToken = function(done){
		var xhr = new XMLHttpRequest();
		var url = this.getBaseHost() + this.getTokenEndPoint();
		xhr.open('GET', url );
		xhr.onload = function() {
			var err = false;
			var token = null;
			if (xhr.status === 200) {
				token = xhr.responseText;
			}
			else {
				err = 'Request failed. Status: ' + xhr.status;
			}
			if(typeof done === "function") done(err, token)
		};
		xhr.send();
	};
	
	Jiraia.prototype._checkSession = function(session, done){
		var xhr = new XMLHttpRequest();
		var url = this.getBaseHost() + this.getTokenEndPoint() + session;
		xhr.open('GET', url);
		xhr.onload = function() {
			var err = null;
			if (xhr.status === 500) {
				err = 'Request failed. Status: ' + xhr.status;
				session = null;
			}
			else if(xhr.status === 404) {
				session = null;   
			}
			if(typeof done === "function") done(err, session)
		};
		xhr.send();
	};
	
	/* @return object */
	Jiraia.prototype._createNewSession = function(done){
		var self = this;
		this.__getToken(function(err, token){
			if(err){
				console.error(err);
			} else {
				if (typeof(window.localStorage) !== "undefined") {
					self.__createLocalSession(token);
				} else {
					self.__createCookiesSession(token);
				}
			}
			if(typeof done === "function") done(err, token);
		});
	};
	
	Jiraia.prototype.getLoggingData = function(session, data){
		
		var host = window.location.host;
		var path = window.location.pathname;
		var userAgent = window.navigator.userAgent;
		var timestamp = (new Date()).toLocaleString();
		var pageTitle = document.title;
		
		var loggingData = {
			host: host,
			path: path,
			userAgent: userAgent,
			timestamp: timestamp,
			pageTitle: pageTitle,
			session: session
		};
		
		var envelope = {
			userSession: loggingData,
			data: [],
		};
		
		if(typeof data !== 'undefined'){
			envelope.data = data;
		}
		
		console.log(envelope);
		return envelope;
		
	};
	
	Jiraia.prototype.saveLoggingData = function(data, done){
		
		var xhr = new XMLHttpRequest();
		var url = this.getBaseHost() + this.getSaveSessionEndPoint() + data.userSession.session;
		xhr.open('POST', url);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		xhr.onload = function() {
			var err = null;
			if (xhr.status !== 204) {
				err = "Status " + xhr.status;
			}
			if(typeof done === "function") done(err)
		};
		xhr.send(JSON.stringify(data));
	};
	
	/* @return void */
	Jiraia.prototype.startLogging = function(session, cb){
		
		this.startCapture();
		
		console.log('saving log data for current session:', session);
		var loggingData = this.getLoggingData(session);
		this.saveLoggingData(loggingData, function(err){
			if(err){
				console.error(err);
			}
			if(typeof cb === "function") cb(err, loggingData);
		});
	};
	
	/* @return void */
	Jiraia.prototype.startCapture = function(){
		
		var options = this.options;
		console.log('options', options);
		if(typeof options === "undefined" || !options.hasOwnProperty('capture')) {
			return;
		}      
		var capture = options.capture;
		
		console.log('found sources', capture);
		
		for(let i=0; i<capture.length; i++){
			this.addCaptureListener(capture[i]);
		}
	};
	
	// O(N)
	Jiraia.prototype.addCaptureListener = function(source){
		console.log('adding capture listener for', source);
		try {
			var self = this;
			var query = source.formSelector + ' ' + source.formDispatcher;
			console.log('query', query);
			document.querySelectorAll(query).forEach((el)=>{
				el.addEventListener('click', function(e){
					console.log('YOU CLICKED ME!');
					self._capture(source);
				});	
			});
		} catch(e){
			console.error(e);
		}
	};
	
	/* @return void */
	Jiraia.prototype._capture = function(source){
		var data = [];
		for(let i=0, query; i<source.sources.length; i++){
			query = source.formSelector + ' ' + source.sources[i].selector;
			data.push({
				model: source.sources[i].model,
				value: document.querySelector(query).value
			});
		}
		console.log('Data',data);
		if(data.length>0) this.saveCapture(data);
	};
	
	Jiraia.prototype.saveCapture = function(data){
		console.log('getLocalSession', this.getLocalSession());
		var logData = this.getLoggingData(this.getLocalSession(), data);
		this.saveLoggingData(logData, (err) => {
			if(err){
				console.error(err);
			}	
		});
	};
	
	/* @return session string */
	Jiraia.prototype.getLocalSession = function(done){
		var session = this._findInStorage();
		if( !session ){
			session = this._findInCookies();
		}
		if(typeof done === "function") done(session);
		return session
	};
	
	/* @return void */
	Jiraia.prototype.startSession = function(done, options){
	  // 1 Check for existent session data
		if(typeof this.options === 'undefined') console.warn('No configuration set.');
		var session = this.getLocalSession();
		var self = this;
		if( !session ){
			console.log('Session NOT found', session);
			try {
				this._createNewSession(function(err, session){
					if(err){
						if(typeof done === "function") done(err);
						return;
					}
					if(typeof done === "function") done(false, session); 
					return;
				});
			} catch(e){
				console.error("Something is preventing from create a new session :(", e);    
				if(typeof done === "function") done(e);
			}
		} else {
			console.log('Session found', session);
			this._checkSession(session, function(err, session){
				if(err){
					if(typeof done === "function") done(err);
					return;
				}
				if(!session){
					self._createNewSession(function(err, session){
						if(err){
							console.error("can't create session. :( ", err);
							if(typeof done === "function") done(err);
							return;
						}
						if(typeof done === "function") done(false, session); 
						return;
					});
				} else {
					if(typeof done === "function") done(false, session);
				}
				return;
			});
		}
	  
	};
	return Jiraia;
})();

// Make it rain!
var jiraia;
(function(){
	try {
		jiraia = new Jiraia;
		jiraia.configure({
			server: 'https://jiraia-backend.herokuapp.com',
			capture:[{
				formSelector: '.newsletter_form',
				formDispatcher: "button.enviar",
				sources : [{
					selector: 'input[name="email"]',
					model: 'email'
				}]
			},{
				formSelector: '.contact_form',
				formDispatcher: "button.enviar",
				sources : [{
					selector: 'input[name="nome"]',
					model: 'nome'
				},{
					selector: 'input[name="email"]',
					model: 'email'
				}]
			}]
		});
		// start session looking for persistent data 
		// or querying api server for a valid one. 
		// it will check against api server too. 
		jiraia.startSession(function(err, sess){
			if(err){
				console.error(err);
				jiraia.nosession();
			}
			// now that sessions are set we start logging activity
			jiraia.startLogging(sess);
		});
	} catch(e){
		console.error("Can't start. Jiraia is dead. :(", e);
	}
})();